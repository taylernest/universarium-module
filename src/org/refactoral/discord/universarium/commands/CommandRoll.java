package org.refactoral.discord.universarium.commands;

import java.util.StringTokenizer;

import org.refactoral.discord.unibot.plugin.Channel;
import org.refactoral.discord.unibot.plugin.ICommand;
import org.refactoral.discord.universarium.Universarium;
import org.refactoral.discord.universarium.internal.QRandom;

public class CommandRoll implements ICommand {
	private QRandom random = new QRandom();

	@Override
	public String getTrigger() {
		return "roll";
	}

	@Override
	public void execute(String fullmsg, String author, Channel channel) {
		StringTokenizer st = new StringTokenizer(fullmsg);
		st.nextToken();
		if(st.hasMoreTokens()) {
			String tok = st.nextToken();
			try {
				int range = Integer.parseInt(tok);
				Universarium.send(random.generate(range) + "", channel.getChannelID());
			} catch (Exception e) {
				return;
			}
		} else {
			return;
		}
	}
	
}