package org.refactoral.discord.universarium.commands;

import org.refactoral.discord.unibot.plugin.Channel;
import org.refactoral.discord.unibot.plugin.ICommand;
import org.refactoral.discord.universarium.Universarium;

public class CommandPing implements ICommand {

	@Override
	public String getTrigger() {
		return "ping";
	}

	@Override
	public void execute(String fullmsg, String author, Channel channel) {
		Universarium.send("Pong!", channel.getChannelID());
	}

}
