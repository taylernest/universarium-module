package org.refactoral.discord.universarium.internal.random;

import java.io.IOException;
import java.util.Random;

public final class QuantumRandom extends Random {

    /**
	 * Eclipse stuff
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean err;
    private IOException exception;

    public QuantumRandom() {
    }

    @Override
    protected int next(int bits) {
        try {
            int bytes = bits % 8 == 0 ? bits / 8 : bits / 8 + 1;
            byte[] bs = getBytes(bytes);
            int ret = 0;
            for (int i = 0; i < bits; i++) {
                int b = bs[i / 8];
                b <<= ((i / 8) * 8);
                int mask = 1 << i;
                ret |= b & mask;
            }
            return ret;
        } catch (IOException ex) {
            err = true;
            exception = ex;
            return super.next(bits);
        }
    }

    private byte[] getBytes(int count) throws IOException {
        return Generation.getBytes(count);
    }

    public boolean err() {
        boolean t = err;
        err = false;
        return t;
    }

    public Exception exception() {
        return exception;
    }
}
