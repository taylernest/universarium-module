package org.refactoral.discord.universarium.internal.random;

import java.io.IOException;

interface ByteFetcher {

	byte nextByte() throws IOException;

	byte[] nextBytes(int count) throws IOException;
}
