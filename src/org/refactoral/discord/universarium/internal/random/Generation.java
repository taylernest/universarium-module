package org.refactoral.discord.universarium.internal.random;

import java.io.IOException;

public class Generation {

    public static final int BUFFER_SIZE = 1 << 16;
    private static ByteFetcher byteFetcher;

    public static void fill(byte[] bytes) throws IOException {
        initFetcher();
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = byteFetcher.nextByte();
        }
    }

    public static byte[] getBytes(int count) throws IOException {
        byte[] bytes = new byte[count];
        fill(bytes);
        return bytes;
    }

    private synchronized static void initFetcher() throws IOException {
        if (byteFetcher == null) {
            byteFetcher = new BufferedByteFetcher();
        }
    }

    private Generation() {
    }
}
