package org.refactoral.discord.universarium.internal;

import java.util.Random;

import org.refactoral.discord.universarium.internal.random.QuantumRandom;

public class QRandom {
	private Random random;
	public QRandom() {
		random = new QuantumRandom();
	}
	public int generate(int range) {
		int i = random.nextInt(range + 1);
		if(i == 0) i++;
		return i;
	}
}
