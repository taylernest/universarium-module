package org.refactoral.discord.universarium;

import org.refactoral.discord.unibot.plugin.BasePlugin;
import org.refactoral.discord.unibot.plugin.LogLevel;
import org.refactoral.discord.universarium.commands.CommandPing;
import org.refactoral.discord.universarium.commands.CommandRoll;

public class Universarium extends BasePlugin {
	public static final String VERSION = "0.1";
	public static final String NAME = "Universarium";
	
	public Universarium() {
		commands.add(new CommandPing());
		commands.add(new CommandRoll());
	}
	
	@Override
	public String getPluginName() {
		return NAME;
	}

	@Override
	public void startEvent() throws Exception {
		log(NAME + " v" + VERSION + " started!", LogLevel.INFO);
	}

	@Override
	public void stopEvent() throws Exception {
	}
}
